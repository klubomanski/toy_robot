require 'spec_helper'

describe Instruction do
  let(:file) { 'spec/fixtures/example.txt' }
  let(:instructions) { Instruction.new(file: file) }

  describe '.new' do
    it 'reads correct file' do
      expect(instructions.file).to eq file
    end
  end

  describe '#parsed' do
    it 'parses the file and returns an array' do
      expect(instructions.parsed)
        .to eq([{ command: 'PLACE', params: '0,0,NORTH' },
                { command: 'MOVE', params: nil },
                { command: 'REPORT', params: nil }])
    end
  end
end
