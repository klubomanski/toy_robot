require 'spec_helper'

describe Robot do
  let(:table) { Table.new }
  let(:direction) { 'NORTH' }
  let(:x) { 0 }
  let(:y) { 0 }
  subject(:robot) { Robot.new(x, y, direction, table) }

  describe '#execute_command' do
    it 'on REPORT' do
      expect(STDOUT).to receive(:puts).with('0, 0, NORTH')
      robot.execute_command('REPORT')
    end

    it 'on MOVE' do
      expect { robot.execute_command('MOVE') }.to change { robot.y }.by(1)
    end

    it 'on LEFT' do
      expect { robot.execute_command('LEFT') }
        .to change { robot.direction }.by(3)
    end

    it 'on RIGHT' do
      expect { robot.execute_command('RIGHT') }
        .to change { robot.direction }.by(1)
    end
  end
end
