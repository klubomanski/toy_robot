## Toy robot

Example implementation of Toy Robot in Ruby


#### Usage

`ruby toy_robot.rb -h` for help

`ruby toy_robot.rb -f FILE_PATH` to run script and add an instructions file

#### Tests

To run specs, use `bundle exec rspec`
