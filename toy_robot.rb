#!/usr/bin/env ruby

require 'optparse'
require_relative 'lib/table'
require_relative 'lib/robot'
require_relative 'lib/instruction'

MANDATORY_OPTIONS = [:file]

options = {}
OptionParser.new do |opts|
  opts.banner = "Usage: ruby robot.rb [options]"

  opts.on('-fFILE', '--file=FILE', '[REQUIRED] Instructions file') do |file|
    options[:file] = file
  end

  opts.on('-xX', '--table_x=X', 'X dimenson for table') do |x|
    options[:x] = x
  end

  opts.on('-yY', '--table_y=Y', 'Y dimention for table') do |y|
    options[:y] = y
  end

  opts.on('-h', '--help', 'Prints this help') do
    puts opts
    exit
  end
end.parse!

begin
  unless MANDATORY_OPTIONS.all?(&options.method(:key?))
    raise OptionParser::MissingArgument.new('Please provide required '\
                                'arguments!')
  end
rescue OptionParser::InvalidOption, OptionParser::MissingArgument
  puts $!.to_s
  puts options
  exit
end

instructions = Instruction.new(file: options[:file]).parsed
table = if options[:x].nil? && options[:y].nil?
        Table.new
      else
        Table.new(x: options[:x], y: options[:y])
      end
instructions.each do |instruction|
  table.execute_instruction(instruction)
end
