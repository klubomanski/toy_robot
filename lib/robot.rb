class Robot
  POSSIBLE_DIRECTIONS = { 0 => 'NORTH', 1 => 'EAST',
                    2 => 'SOUTH', 3 => 'WEST' }.freeze
  private_constant :POSSIBLE_DIRECTIONS

  attr_reader :x, :y, :direction, :table

  def initialize(x, y, direction, table)
    @x = x.to_i
    @y = y.to_i
    @direction = POSSIBLE_DIRECTIONS.key(direction)
    @table = table
  end

  def execute_command(command)
    case command
    when 'MOVE'
      move
    when 'LEFT'
      rotate(:left)
    when 'RIGHT'
      rotate(:right)
    when 'REPORT'
      puts current_state
    end
  end

  private

  # Moves the robot in a given direction
  def move
    case @direction
    when 0
      return unless @table.can_be_placed?(@x, @y + 1)
      @y += 1
    when 1
      return unless @table.can_be_placed?(@x + 1, @y)
      @x += 1
    when 2
      return unless @table.can_be_placed?(@x, @y - 1)
      @y -= 1
    when 3
      return unless @table.can_be_placed?(@x -1, @y)
      @x -= 1
    end
  end

  # Rotate in a given direction
  #
  # @param direction [Symbol]
  def rotate(direction)
    if direction == :left
      @direction == 0 ? @direction = 3 : @direction -= 1
    elsif direction == :right
      @direction == 3 ? @direction = 0 : @direction += 1
    end
  end

  # Returns robots' current position and direction
  def current_state
    "#{@x}, #{@y}, #{POSSIBLE_DIRECTIONS[@direction]}"
  end
end
