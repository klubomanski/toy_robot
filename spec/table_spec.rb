require 'spec_helper'

describe Table do
  let(:table) { Table.new }
  let(:with_robot) do
    t = Table.new
    t.instance_variable_set(:@robot, Robot.new(0,0,'NORTH', table))
  end

  describe '.new' do
    it 'with default attributes' do
      expect(table.x).to eq(5)
      expect(table.y).to eq(5)
    end
  end

  describe '#execute_instruction' do
    context 'when instruction is PLACE' do
      let(:instruction) { { command: 'PLACE', params: '0,0,NORTH' } }
      let(:invalid_instruction) do
        { command: 'PLACE', params: '10,10,NORTH' }
      end

      it 'initializes robot, when robot is not present' do
        expect(Robot).to receive(:new)
        table.execute_instruction(instruction)
      end

      it 'places the robot if robot is present' do
        table.instance_variable_set(:@robot, Robot.new(0,0,'NORTH', table))
        expect(Robot).to receive(:new)
        table.execute_instruction(instruction)
      end

      it 'doesn\'t place the robot if params are bigger then table' do
        table.execute_instruction(invalid_instruction)
        expect(table.robot).to be_nil
      end
    end

    it 'executes command' do
      table.instance_variable_set(:@robot, Robot.new(0,0,'NORTH', table))
      expect(table.execute_instruction({ command: 'LEFT' })).to eq 3
    end
  end

  describe '#can_be_placed?' do
    it 'returns true if params are in range' do
      expect(table.can_be_placed?(2, 3)).to be true
    end

    it 'returns false if params are not in range' do
      expect(table.can_be_placed?(10, 3)).to be false
    end
  end
end
