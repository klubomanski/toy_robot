class Instruction
  attr_reader :file

  def initialize(file:)
    @file = file
  end

  def parsed
    File.readlines(file).map do |instruction|
      c = instruction.gsub("\n", '').split
      { command: c[0], params: c[1] }
    end
  end
end
