class Table
  attr_reader :x, :y, :robot, :maximal

  def initialize(x: 5, y: 5)
    @x = x || 5
    @y = y || 5
    @maximal = { x: @x - 1, y: @y - 1 }
  end

  def execute_instruction(instruction)
    if !@robot || @robot && instruction[:command] == 'PLACE'
      return init_robot(instruction) if instruction[:command] == 'PLACE'
    else
      @robot.execute_command(instruction[:command])
    end
  end

  def can_be_placed?(robot_x, robot_y)
    x_exceeded?(robot_x) && y_exceeded?(robot_y) ? true : false
  end

  private

  def init_robot(instruction)
    params = instruction[:params].split(',')
    if can_be_placed?(params[0].to_i, params[1].to_i)
      @robot = Robot.new(params[0], params[1], params[2], self)
    end
  end

  def x_exceeded?(robot_x)
    @maximal[:x] >= robot_x && robot_x >= 0
  end

  def y_exceeded?(robot_y)
    @maximal[:y] >= robot_y && robot_y >= 0
  end
end
